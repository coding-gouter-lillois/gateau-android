package org.coding_gouter_lillois.gateau;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Game extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
    }

    public void checkGame(View view) {
        boolean win = true;
        win = checkGoodIngredients(win);
        win = checkBadIngredients(win);

        Toast toast;
        if (win) {
            toast = Toast.makeText(getApplicationContext(), "Gagné !", Toast.LENGTH_SHORT);
        } else {
            toast = Toast.makeText(getApplicationContext(), "Perdu !", Toast.LENGTH_SHORT);
        }
        toast.show();

    }

    private boolean checkGoodIngredients(boolean win) {
        List<Integer> goodIngredients = new ArrayList<>();
        goodIngredients.add(R.id.egg);
        goodIngredients.add(R.id.sugar);
        goodIngredients.add(R.id.floor);
        goodIngredients.add(R.id.chocolat);
        goodIngredients.add(R.id.vanilla);
        goodIngredients.add(R.id.butter);
        for (Integer ingredient : goodIngredients) {
            if (!check(ingredient)) win = false;
        }
        return win;
    }

    private boolean checkBadIngredients(boolean win) {
        List<Integer> badIngredients = new ArrayList<>();
        badIngredients.add(R.id.wood);
        badIngredients.add(R.id.chicken);
        badIngredients.add(R.id.apple);
        badIngredients.add(R.id.vinegar);
        badIngredients.add(R.id.tea);
        badIngredients.add(R.id.yeast);
        badIngredients.add(R.id.ash);
        badIngredients.add(R.id.garlic);
        badIngredients.add(R.id.banana);
        badIngredients.add(R.id.caramel);
        badIngredients.add(R.id.ketchup);
        for (Integer ingredient : badIngredients) {
            if (check(ingredient)) win = false;
        }
        return win;
    }

    private Boolean check(int wood2) {
        CheckBox wood = findViewById(wood2);
        return wood.isChecked();
    }
}
